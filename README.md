Agree
-----

An interactive software license generator, written in Go. (golang)

Install: `go get rjl.li/agree`

Currently supports the following license types:
- unlicense
- mit
- apache
- agpl
- gpl
- lgpl
- mozilla

Writes out to LICENSE.md, or custom filename.
Takes user input to fill license templates, no more copy/pasting and searching for places to insert your name or the year, etc.

The library powering the tool is [license](http://rjl.li/license). If you want to further that project or add a license that is not yet supported, pull requests are welcome.


© 2017  Rob J loranger  [hello@robloranger.ca](maileto://hello@robloranger.ca)
